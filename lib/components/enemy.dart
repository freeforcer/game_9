import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:game_9/game_controller.dart';

class Enemy {
  final GameController gameController;
  Rect rect;
  double speed = 10;
  Enemy(this.gameController, double x, double y) {
    rect = Rect.fromLTWH(
      x,
      y,
      20,
      20,
    );
  }

  void render(Canvas c) {
    c.drawRect(rect, Paint()..color = Colors.red);
  }

  void update(double t){
    double stepDistance = speed * t;
    Offset toPlayer = gameController.player.rect.center - rect.center;
    if (stepDistance < toPlayer.distance) {
      Offset stepToPlayer = Offset.fromDirection(toPlayer.direction, stepDistance);
      rect = rect.shift(stepToPlayer);
    }
  }
}