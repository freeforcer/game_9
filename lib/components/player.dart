import 'dart:ui';

import 'package:flutter/material.dart';

import '../game_controller.dart';

class Player {
  final GameController gameController;
  Rect rect;

  Player(this.gameController) {
    rect = Rect.fromLTWH(
      gameController.screenSize.width / 2,
      gameController.screenSize.height / 2,
      40,
      40,
    );
  }

  void render(Canvas c) {
    c.drawRect(rect, Paint()..color = Colors.green);
  }

  void update(double t){

  }
}