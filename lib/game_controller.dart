import 'dart:ui';

import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flutter/gestures.dart';

import 'components/enemy.dart';
import 'components/player.dart';

class GameController extends Game {
  Size screenSize;
  Player player;
  Enemy enemy;

  GameController() {
    initialize();
  }

  void initialize() async {
    resize(await Flame.util.initialDimensions());
    player = Player(this);
    enemy = Enemy(this, 200, 200);
  }

  void render(Canvas c){
    Rect background = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    Paint bp = Paint()..color = Color(0xFF0000FF);
    c.drawRect(background, bp);

    player.render(c);
    enemy.render(c);
  }

  void update(double t) {
    enemy.update(t);
  }

  void resize(Size size) {
    screenSize = size;

  }

  void onTapDown(TapDownDetails d) {
    print(d.globalPosition);
  }
}